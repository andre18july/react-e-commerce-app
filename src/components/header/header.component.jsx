import React from "react";
import { auth } from "../../firebase/firebase.utils";
import CartIcon from "../cart-icon/cart-icon.component";
import CartDropdown from "../cart-dropdown/cart-dropdown.component";
import { createStructuredSelector } from "reselect";
import { selectCurrentUser } from "../../redux/user/user.selectors";
import { selectCartHidden } from "../../redux/cart/cart.selectors";

import { ReactComponent as Logo } from "../../assets/crown.svg";
import { connect } from "react-redux";

import {
  HeaderContainer,
  LogoContainer,
  OptionsContainer,
  OptionNavLink
} from "./header.styles";

const Header = ({ currentUser, hidden }) => {
  return (
    <HeaderContainer>
      <LogoContainer to="/">
        <Logo className="logo" />
      </LogoContainer>
      <OptionsContainer>
        <OptionNavLink to="/shop">
          SHOP
        </OptionNavLink>
        <OptionNavLink to="/contact">
          CONTACT
        </OptionNavLink>

        {currentUser ? (
          <OptionNavLink as='div' onClick={() => auth.signOut()}>
            SIGN OUT
          </OptionNavLink>
        ) : (
          <OptionNavLink
            to="/signin"
          >
            SIGN IN
          </OptionNavLink>
        )}

        <CartIcon />
      </OptionsContainer>
      {hidden ? null : <CartDropdown />}
    </HeaderContainer>
  );
};

/* Syntax 1*/
const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser,
  hidden: selectCartHidden
});

/*Syntax 2*/
/*
const mapStateToProps = state => ({
  currentUser: selectCurrentUser(state),
  hidden: selectCartHidden(state)
}); 
*/

export default connect(mapStateToProps)(Header);
