import React from "react";
import "./checkout-item.styles.scss";
import { connect } from "react-redux";
import { clearItem, addItem, subItem } from "../../redux/cart/cart.action";

const CheckoutItem = ({ cartItem, clearItem, addItem, subItem }) => {
  const { id, imageUrl, name, price, quantity } = cartItem;

  return (
    <div className="checkout-item">
      <div className="image-container">
        <img src={imageUrl} alt={name} />
      </div>
      <span className="name">{name}</span>
      <span className="price">{price}</span>
      <span className="quantity">
        <div className="arrow" onClick={() => subItem(cartItem)}>
          &#10094;
        </div>
        <span className="value">{quantity}</span>
        <div className="arrow" onClick={() => addItem(cartItem)}>
          &#10095;
        </div>
      </span>
      <div className="remove-button" onClick={() => clearItem(id)}>
        &#10005;
      </div>
    </div>
  );
};

const mapDisptachToProps = disptach => ({
  clearItem: id => disptach(clearItem(id)),
  addItem: item => disptach(addItem(item)),
  subItem: item => disptach(subItem(item))
});

export default connect(
  null,
  mapDisptachToProps
)(CheckoutItem);
