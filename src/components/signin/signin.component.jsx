import React, { Component } from "react";
import InputForm from "../form-input/form-input.component";
import CustomButton from '../custom-button/custom-button.component';
import { auth, signInWithGoogle } from '../../firebase/firebase.utils';

import "./signin.styles.scss";


class Signin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: ""
    };
  }



  handleSubmit = async event => {
    event.preventDefault();

    const { email, password } = this.state;

    try {
      await auth.signInWithEmailAndPassword(email, password);
      
      this.setState({ email: '', password: '' });
    } catch (error) {
      console.log(error);
    }
  };




  handleChange = event => {
    const { value, name } = event.target;

    this.setState({
      [name]: value
    });
  };

  googleSignIn(){

    signInWithGoogle();

  }

  render() {
    return (
      <div className="signin">
        <h2>I already have an account</h2>
        <span>Sign in with your email and password</span>

        <form onSubmit={this.handleSubmit}>
          <InputForm
            id="email"
            type="email"
            name="email"
            label="Email"
            value={this.state.email}
            handleChange={this.handleChange}
            required
          />
          <InputForm
            id="password"
            type="password"
            name="password"
            label="Password"
            value={this.state.password}
            handleChange={this.handleChange}
            required
          />
          <div className='buttons'>
            <CustomButton type='submit'>Sign In</CustomButton>
            <CustomButton onClick={this.googleSignIn} isGoogleSignIn>Google Signin</CustomButton>
          </div>
        </form>
      </div>
    );
  }
}

export default Signin;
