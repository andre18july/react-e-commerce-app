export const CartActionTypes = {
    TOGGLE_CART_HIDDEN: 'TOGGLE_CART_HIDDEN',
    ADD_ITEM: 'ADD_ITEM',
    SUB_ITEM: 'SUB_ITEM',
    CLEAR_ITEM: 'CLEAR_ITEM'
};