export const addIemToCart = (cartItems, cartItemToAdd) => {
  const checkIfItemExists = cartItems.find(
    cartItem => cartItem.id === cartItemToAdd.id
  );

  if (checkIfItemExists) {
    return cartItems.map(cartItem =>
      cartItem.id === cartItemToAdd.id
        ? { ...cartItem, quantity: cartItem.quantity + 1 }
        : cartItem
    );
  }

  return [...cartItems, { ...cartItemToAdd, quantity: 1 }];
};

export const clearItemFromCart = (cartItems, cartItemToRemoveId) => {
  const updatedCartItems = cartItems.filter(cartItem => {
    return cartItem.id !== cartItemToRemoveId;
  });
  return updatedCartItems;
};

export const subItemFromCart = (cartItems, cartItemToSub) => {
  const existingCarItem = cartItems.find(
    cartItem => cartItem.id === cartItemToSub.id
  );

  if (existingCarItem.quantity === 1) {
    return cartItems.filter(carItem => carItem.id !== cartItemToSub.id);
  } else if (existingCarItem.quantity > 1) {
    return cartItems.map(cartItem =>
      cartItem.id === cartItemToSub.id
        ? { ...cartItem, quantity: cartItem.quantity - 1 }
        : cartItem
    );
  }

  return cartItems;
};
