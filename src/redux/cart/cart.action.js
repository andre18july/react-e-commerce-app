import { CartActionTypes } from './cart.types';

export const toggleCartHidden = () => ({
    type: CartActionTypes.TOGGLE_CART_HIDDEN
});

export const addItem = item => ({
    type: CartActionTypes.ADD_ITEM,
    payload: item
});

export const subItem = item => ({
    type: CartActionTypes.SUB_ITEM,
    payload: item
});

export const clearItem = itemId => {

    return ({
    type: CartActionTypes.CLEAR_ITEM,
    payload: itemId
})};