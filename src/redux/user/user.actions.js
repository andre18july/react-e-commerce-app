import { UserActionTypes } from './user.types';

export const setCurrentUser = _user => ({
    type: UserActionTypes.SET_CURRENT_USER,
    payload: _user
})